class UrlsController < ApplicationController
  before_action :set_url, only: %i[show stats]

  # GET /urls/:short_url
  def show
    @url.increment(:stats)
    render json: @url
  end

  # POST /urls
  def create
    result = Urls::Create.call(original: params[:original])
    @url = result.url

    render json: @url, status: :created
  end

  # GET /urls/:short_url/stats
  def stats
    render json: @url
  end

  private

  def set_url
    @url = Url.find_by!(short: params[:short])
  end
end
