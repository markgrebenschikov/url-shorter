module Urls
  class Create
    include Interactor
    include Interactor::Contracts

    expects do
      required(:original).filled
    end

    delegate :original, to: :context

    def call
      url = Url.find_by(original: original)

      if url.blank?
        url = Url.create(
          original: original,
          short: SecureRandom.hex.first(8)
        )
      end

      context.url = url
    end
  end
end
