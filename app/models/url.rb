class Url < ApplicationRecord
  validates :original, :short, presence: true, uniqueness: true
end
