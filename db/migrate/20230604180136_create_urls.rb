class CreateUrls < ActiveRecord::Migration[7.0]
  def change
    create_table :urls do |t|
      t.string :original, unique: true
      t.string :short, unique: true
      t.integer :stats, default: 0

      t.timestamps
    end
  end
end
