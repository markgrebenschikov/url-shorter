Rails.application.routes.draw do
  resources :urls, only: [:show, :create], param: :short do
    member do
      get :stats
    end
  end
end
